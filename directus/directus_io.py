import logging
import os
import pandas as pd
import numpy as np
import requests
from dotenv import load_dotenv
import re

load_dotenv()


class CmsClient:
    def __init__(self, directus_url=None, directus_email=None, directus_password=None):
        """
        Client for connection to the CMS
        Args:
            directus_url (str): API endpoint directus
            directus_email (str): Email for logging in to Directus
            directus_password (str): Password for Directus
        """
        self.directus_url = directus_url if directus_url else os.environ['CMS_URL']
        self.directus_email = directus_email if directus_email else os.environ['CMS_USERNAME']
        self.directus_password = directus_password if directus_password else os.environ['CMS_PASSWORD']
        self.token = ""
        self.authenticate()
        logging.info('Connected to {}'.format(self.directus_url))

    def authenticate(self):
        """Authenticates in directus

        Notes: Measured execution time: 613 ms ± 37.2 ms

        Returns:
            token (str): authentication string
        """
        auth_url = self.directus_url + 'auth/authenticate'

        auth_res = requests.post(
            url=auth_url,
            data={
                'email': self.directus_email,
                'password': self.directus_password,
            }
        )
        if auth_res.status_code != 200:
            print(auth_res.text)
            raise Exception(auth_res.text)

        self.token = auth_res.json()['data']['token']

    def get_items(self, collection_name='fact', **kwargs):
        item_url = self.directus_url + 'items/' + collection_name
        item_res = requests.get(
            url=item_url,
            headers={
                'Authorization': 'Bearer {}'.format(self.token)
            },
            params={**kwargs}
        )
        if item_res.status_code == 102:
            self.authenticate()
            item_res = requests.get(
                url=item_url,
                headers={
                    'Authorization': 'Bearer {}'.format(self.token)
                },
                params={**kwargs}
            )

        if item_res.status_code != 200:
            raise Exception(item_res.text)
        return item_res.json()

    def create_items(self, collection_name='fact', **kwargs):
        item_url = self.directus_url + 'items/' + collection_name
        item_res = requests.post(
            url=item_url,
            headers={
                'Authorization': 'Bearer {}'.format(self.token)
            },
            json=kwargs
        )
        if item_res.status_code == 102:
            self.authenticate()
            item_res = requests.post(
                url=item_url,
                headers={
                    'Authorization': 'Bearer {}'.format(self.token)
                },
                json=kwargs
            )

        if item_res.status_code != 200:
            raise Exception(item_res.text)
        return item_res.json()

    def update_items(self, collection_name='fact', **kwargs):
        item_url = self.directus_url + 'items/' + collection_name + "/" + str(kwargs['id'])
        del kwargs['id']
        self.authenticate()
        item_res = requests.patch(
            url=item_url,
            headers={
                'Authorization': 'Bearer {}'.format(self.token)
            },
            json=kwargs
        )
        if item_res.status_code == 102:
            self.authenticate()
            item_res = requests.patch(
                url=item_url,
                headers={
                    'Authorization': 'Bearer {}'.format(self.token)
                },
                json=kwargs
            )

        if item_res.status_code != 200:
            raise Exception(item_res.text)
        return item_res.json()

    def delete_items(self, collection_name='fact', **kwargs):
        item_url = self.directus_url + 'items/' + collection_name + "/" + str(kwargs['id'])
        del kwargs['id']
        item_res = requests.delete(
            url=item_url,
            headers={
                'Authorization': 'Bearer {}'.format(self.token)
            },
        )
        if item_res.status_code == 102:
            self.authenticate()
            item_res = requests.patch(
                url=item_url,
                headers={
                    'Authorization': 'Bearer {}'.format(self.token)
                },
            )

        if item_res.status_code != 200:
            raise Exception(item_res.text)
        return item_res.json()


class CmsReader(object):

    def __init__(self, cms_client):
        self.cms_client = cms_client

        self.relevant_fields = {
            "complaint": ["id", "triage_label", "isabel_terms"],
            "question_set": ["id", "complaint_id"],
            "question_set_sequence": ["id", "question_set_id", "question_id", "sequence", "native", "is_relevant_if"],
            "question": ["id", "is_relevant_if", "label", "question_source", "information", "question_type_id",
                         "answer_set_id", "min", "max", "slider_steps"],
            "answer_set": ["id", "label"],
            "answer_set_option": ["id", "answer_set_id", "label"],
            "fact": ["id", "label", "fact_triagewijzer", "question_id", "answer_condition", "answer_set_option_id",
                     "isabel_terms"],
            "question_type": ["id", "key"]
        }

        self.complaint = self.build_dataframe('complaint')
        self.question_set = self.build_dataframe('question_set')
        self.question_set_sequence = self.build_dataframe('question_set_sequence')
        self.question = self.build_dataframe('question')
        self.answer_set = self.build_dataframe('answer_set')
        self.answer_set_option = self.build_dataframe('answer_set_option')
        self.fact = self.build_dataframe('fact')
        self.question_type = self.build_dataframe('question_type')
        self.urgency_rules = self.get_urgency_rules()
        self.question_base = pd.DataFrame()
        self.set_question_base()

        # Only include questions that are in the base file.
        self.question = self.question[self.question["question_id"].isin(self.question_base["question_id"].tolist())]

        self.question_set_sequence = self.question_set_sequence[
            self.question_set_sequence["question_set_sequence_id"].isin(
                self.question_base["question_set_sequence_id"].tolist())]

    def build_dataframe(self, collection_name):
        """
        Get the relevant fields from questionset and return it as a dataframe
        Returns:

        """
        data = self.cms_client.get_items(collection_name=collection_name, limit=-1)['data']
        fields = self.relevant_fields[collection_name]
        df = pd.DataFrame(data)[fields]

        # the column name "id" should be renamed to contain the collection name
        fields[0] = collection_name + "_id"

        # the column name "label" should be renamed to contain the collection name
        fields = [field if field != "label" else field + "_" + collection_name for field in fields]

        # Isabel terms can exist in multiple collections
        fields = [field if field != "isabel_terms" else field + "_" + collection_name for field in fields]

        # is_relevant_if can exist in 2 levels
        fields = [field if field != "is_relevant_if" else field + "_" + collection_name for field in fields]

        df.columns = fields
        return df

    def set_question_base(self):
        """
        Download the base question file
        Returns:

        """
        self.question_base = self.question_set.copy()#.fillna(-1)  # Start with question set
        self.question_base['complaint_id'] = self.question_base['complaint_id'].fillna(-1).astype(int)
        self.question_base = self.complaint.merge(self.question_base, on='complaint_id', how='outer')  # + complaint
        self.question_base = self.question_base.merge(self.question_set_sequence, on='question_set_id', how='outer')
        self.question_base = self.question_base.merge(self.question, on='question_id', how='outer')
        self.question_base = self.question_base.merge(self.question_type, on='question_type_id', how='outer')
        self.question_base = self.question_base.merge(self.answer_set, on='answer_set_id', how='outer')
        self.question_base = self.question_base.merge(self.answer_set_option, on='answer_set_id', how='outer')
        self.question_base = self.question_base.merge(self.fact, on=['question_id', 'answer_set_option_id'], how='outer')

        # Make id columns integer
        colnames = self.question_base.columns.tolist()
        for col in colnames:
            if col[-3:] == "_id":
                self.question_base[col] = self.question_base[col].fillna(-1).astype(int)

        self.question_base["sequence"] = self.question_base["sequence"].fillna(-1).astype(int)
        self.question_base = self.question_base.sort_values(by=['complaint_id', 'question_set_id', 'sequence'])

    def find_fact(self, factstr):
        """
        Convert a string of facts e.g. f_1921 && f_1922 to a list of fact_id's
        Args:
            factstr: string containing the logical expression: e.g. "f_1921 && f_1922"

        Returns:
            facts_ids: list, e.g. [1921, 1922]

        """
        facts = re.findall("f_[0-9]+", factstr)
        facts_int = [f[2:] for f in facts]
        return facts_int

    def get_urgency_rules(self, explode=False):
        """
        Builds a dataframe with urgency rules
        Returns:
            Exploded dataframe of urgencies
        """
        urgency = self.cms_client.get_items('urgency_rule', limit=-1)
        df = pd.DataFrame(urgency['data'])
        df = df[['id', 'urgency_id', 'complaint_id', 'rule']]
        df['id'] = df['id'].astype(int)
        df = df.merge(self.complaint[['complaint_id', 'triage_label']], on='complaint_id', how='left')
        if explode:
            df = df.explode('fact_id')
        return df

    def store_snapshot(self):
        """
        Save results of CMS download.
        Returns:

        """
        export_folder = os.environ["EXPORT_FOLDER"]

        # Export Question_base containing all the information for the question paths
        self.question_base.to_csv(export_folder + "question_base_from_cms.csv", index=False, sep=';')

        # Export complaint for maintaining the base urgency for each complaint
        complaint_tmp = self.complaint.copy()
        complaint_tmp['base_urgency'] = '-'
        complaint_tmp.to_csv(export_folder + "chief_complaint_urgencies.csv", index=False, sep=';')

        # Export urgency_rules containing the urgency_rules as data
        self.urgency_rules.to_csv(export_folder + "urgency_rule_set.csv", index=False, sep=';')

class CmsWriter(object):

    def __init__(self):
        pass